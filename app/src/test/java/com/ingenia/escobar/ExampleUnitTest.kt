package com.ingenia.escobar

import com.ingenia.escobar.app.getCedula
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun check_cedula_iscorrectformat(){
        val cedula: String = "020676104204B         031074178292      152415380051853850TOCANCIPA              RODRIGUEZ              GLORIA                 ADRIANA                0F19670313026001O+ 2\u0081¹\u0090\u008A\u009F\u0095\u0087}¥\u0095\u0092\u0098\u0082\u008B¯v\u0082\u0094\u0085mµ\u0094¹~z\u008E\u0080\u009Au\u0081½\u0092¤©wsr\u0090¤[|¥\u008E²\u007F[±X¯³»¬gp¬Qp`Â«Ò\u0088\u0094K\u009F¿Òs\u008A¿À´×\u0087\u0080½zM\u0097ÃÑ¥\u0093CÜvvHÛ\u009E»ÄY¥P\u008DÕ²P\u009BY²K\u0084\u00966ç\u0081Ï½z:\u008DÔ\u008A5²û\u0010²¬)½\u000BÉ¦\u0083ïª\u008C©±<°B¤ÑA\u0015/Jì2±kðW&²D\u009Ar¢\u0092VÏ\u0006taM7\u0081¹{{\u008Amx\u0092\u0088¥µ\u007F\u0094cwp\u008Fb\u0081¨m\u0081or¼x¡\\\u009AZ¿\u0088µ£g\u008Ef\u0090v\u00ADÁ\u0099Äy¾£i¤¶`\u0090S\u0089ºÉ\u007F_\u0099sXY|Ê\u009C\u0085Jqºg²Ñ\u008AÇdl¸Ó\u0080\u009EÇ¦Æ\u0094É\u0085ÈP\u008CÀºjOÖ\u0098\u008F?·GÛy¬@¹ÆoDß\u0092³ËÎVÑ³ÄI²êÏ¸óp¶«/Ë+Î®\u009B\u000F>Ú\u0090C\u000BçJ\u008BÒº¬M±\u0002î6â\u000FU\u0014\u008E8û\u0095BÓo0Ê0\u000F\u0002o\u0012ÿª\\¡\u0096ÓxJ(\u0016\u0012¾J2Æ\u0014d\u0089º«¿îlïuZ5úý\u0001\u000E? JA"
        assertEquals("51853850", cedula.getCedula())
    }
}