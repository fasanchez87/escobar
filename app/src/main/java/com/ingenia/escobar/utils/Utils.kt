package com.ingeniapps.encargauser.utils

import org.json.JSONObject
import retrofit2.HttpException


fun handleErrorAPI(exception: HttpException): String{
    val message: String?
    message = if(exception.code() != 500){
        val jsonObject: JSONObject = JSONObject(exception.response()?.errorBody()?.string())
        "${jsonObject.opt("message")}"
    }
    else{
        "Error interno en el servidor, estamos trabajando en ello."
    }
    return message
}

/*fun getHashKeyFacebook(context: Context, packagename: String){
    try {
        val info = context.packageManager.getPackageInfo(
            packagename,  //Insert your own package name.
            PackageManager.GET_SIGNATURES
        )
        for (signature in info.signatures) {
            val md: MessageDigest = MessageDigest.getInstance("SHA")
            md.update(signature.toByteArray())
            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
        }
    }
    catch (e: PackageManager.NameNotFoundException) {
    }
    catch (e: NoSuchAlgorithmException) {
    }
}*/

