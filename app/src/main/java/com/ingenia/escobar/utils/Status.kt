package com.ingeniapps.encargauser.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}