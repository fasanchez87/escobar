package com.ingenia.escobar.model

data class Sitio(val nomSitio: String?, val maxPersonas: Int, val aforo: Int)