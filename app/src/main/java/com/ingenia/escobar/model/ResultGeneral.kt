package com.ingenia.escobar.model

import com.google.gson.annotations.SerializedName

data class ResultGeneral(val status: Boolean = false, val message: String = "", @SerializedName("datos") val data: Data)