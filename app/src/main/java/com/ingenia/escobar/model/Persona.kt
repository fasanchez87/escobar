package com.ingenia.escobar.model

import java.io.Serializable

data class Persona(val codPersona: String,
                   val numDocumento: String,
                   val nomPersona: String,
                   val apePersona: String,
                   val codSangre: String,
                   val celPersona: String,
                   val nomFuncion: String,
                   val nomContratista: String,
                   val imgPersona: String): Serializable