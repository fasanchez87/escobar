package com.ingenia.escobar.model

import com.google.gson.annotations.SerializedName

data class ResultReadQR(val status: Boolean = false,
                        val message: String = "",
                        val typeIngreso: String = "",
                        @SerializedName("sitio") val sitio: Sitio,
                        @SerializedName("persona") val persona: Persona?)