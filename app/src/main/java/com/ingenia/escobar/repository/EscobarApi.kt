package com.ingenia.escobar.repository

import com.ingenia.escobar.model.ResultGeneral
import com.ingenia.escobar.model.ResultReadQR
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface EscobarApi {

    @GET("/escobar/ws/login")
    suspend fun login(@Header("user") name: String?,
                      @Header("pass") tokenFCM: String?): ResultGeneral

    @POST("/escobar/ws/qr")
    suspend fun readQR(@Header("codUsuario") codUsuario: String?,
                       @Header("codQr") codQr: String?,
                       @Header("indManual") indManual: Int): ResultReadQR
}