package com.ingenia.escobar.repository

import com.ingenia.escobar.app.Injection

object RemoteRepository : Repository {

    private val api = Injection.provideEncargaApi()

    override suspend fun login(user: String?, pass: String?) = api.login(user, pass)
    override suspend fun readQR(codUsuario: String?, codQr: String?, indManual: Int) = api.readQR(codUsuario, codQr, indManual)
}