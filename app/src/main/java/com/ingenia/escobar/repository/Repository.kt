package com.ingenia.escobar.repository

import com.ingenia.escobar.model.ResultGeneral
import com.ingenia.escobar.model.ResultReadQR

interface Repository {
    suspend fun login(user: String?, pass: String?): ResultGeneral
    suspend fun readQR(codUsuario: String?, codQr: String?, indManual: Int): ResultReadQR
}