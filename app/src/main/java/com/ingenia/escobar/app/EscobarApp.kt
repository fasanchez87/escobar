package com.ingenia.escobar.app

import android.app.Application
import android.content.Context
import android.content.SharedPreferences

class EscobarApp: Application() {

    companion object {
        private lateinit var instance: EscobarApp
        fun getAppContext(): Context = instance.applicationContext

        //GESTION SHAREDPREFERENCES
        private lateinit var preferences: SharedPreferences
        private lateinit var loginPrefsEditor: SharedPreferences.Editor

        fun putSharedString(key: String?, value: String?) {
            checkForNullKey(key)
            checkForNullValue(value)
            preferences.edit().putString(key, value).apply()
        }

        fun getSharedString(key: String?): String? {
            return preferences.getString(key, "")
        }

        fun checkForNullKey(key: String?) {
            if (key == null) {
                throw NullPointerException()
            }
        }

        fun checkForNullValue(value: String?) {
            if (value == null) {
                throw NullPointerException()
            }
        }

        fun putSharedBoolean(key: String?, value: Boolean) {
            checkForNullKey(key)
            preferences.edit().putBoolean(key, value).apply()
        }

        fun getSharedBoolean(key: String?): Boolean {
            return preferences.getBoolean(key, false)
        }

        fun clearSharedPreferences() = preferences.edit().clear().apply()

        fun removeSharedString(key: String?) {
            preferences.edit().remove(key).apply()
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        preferences = instance.getSharedPreferences("loginPrefs", Context.MODE_PRIVATE)
        loginPrefsEditor = preferences.edit()
      /*  //PENDIENTE
        Crashlytics.setUserIdentifier("3236261692")
        Crashlytics.setUserName("alex")
        Crashlytics.setUserEmail("99999999@GMAIL.COM")*/
    }
}