package com.ingenia.escobar.app

import com.ingenia.escobar.BuildConfig
import com.ingenia.escobar.repository.EscobarApi
import com.ingenia.escobar.repository.RemoteRepository
import com.ingenia.escobar.repository.Repository
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Injection {

    fun provideRepository(): Repository = RemoteRepository

    private fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(provideOkhhtpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun provideEncargaApi(): EscobarApi {
        return provideRetrofit().create(EscobarApi::class.java)
    }

    private fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = if(BuildConfig.DEBUG){
            HttpLoggingInterceptor.Level.BODY
        }
        else{
            HttpLoggingInterceptor.Level.NONE
        }
        return logging
    }

    private fun provideOkhhtpClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(provideLoggingInterceptor())
                  .callTimeout(10, TimeUnit.SECONDS)
                  .connectTimeout (10, TimeUnit.SECONDS)
                  .readTimeout (30, TimeUnit.SECONDS)
                  .writeTimeout (30, TimeUnit.SECONDS)
        return httpClient.build()
    }
}