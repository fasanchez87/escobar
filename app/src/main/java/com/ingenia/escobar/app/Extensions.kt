package com.ingenia.escobar.app

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.*
import android.os.Build
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.afollestad.materialdialogs.MaterialDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.ingenia.escobar.R
import com.ingenia.escobar.app.EscobarApp.Companion.clearSharedPreferences
import com.ingenia.escobar.app.EscobarApp.Companion.getAppContext
import com.ingenia.escobar.ui.login.Login
import com.ingenia.escobar.ui.manualscanner.ManualQR
import com.ingenia.escobar.ui.scanner.Scanner
import org.jetbrains.anko.startActivity
import java.math.BigInteger
import java.security.MessageDigest
import java.util.*

fun toast(message: String?) {
    Toast.makeText(EscobarApp.getAppContext(), message, Toast.LENGTH_LONG).show()
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}

fun String.isValidEmail(): Boolean {
    return !TextUtils.isEmpty(this) && Patterns.EMAIL_ADDRESS.matcher(this).matches()
}


fun ImageView.loadImage(url: String?){
    Glide.with(this)
        .load(url)
        .override(1000,1400)
        .apply(RequestOptions.placeholderOf(R.color.colorGrisSuave))
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(this)
}

fun String.getCedula(): String{
    return subSequence(48,58).replaceFirst("^0+(?!$)".toRegex(), "")
}

fun String.isStringQR(): Boolean{
    return length < 150
}

fun Activity.setWindowFullScreen(){
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        val w = window
        w.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
    }
}

fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
}

fun AppCompatActivity.initAppBar(){
    val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
    setSupportActionBar(toolbar)
    supportActionBar!!.title = ""
}

fun Context.showDialog(message: String?){
    val dialog = MaterialDialog(this)
        .icon(R.drawable.image_dialog)
        .title(text = message)
        .cancelable(false)
        .positiveButton(text = "Aceptar"){

        }
    dialog.show()
}

fun Context.showDialogCloseSession(message: String?, activity: Activity){
    MaterialDialog(this)
        .icon(R.drawable.image_dialog)
        .title(text = message)
        .show {
            positiveButton(text = "Aceptar") {
                clearSharedPreferences()
                startActivity<Login>()
                activity.finish()
            }
            negativeButton(text = "Cancelar") {
            }
        }
}

fun <T> ImageView.loadCircularImage(
    model: T,
    borderSize: Float = 12F,
    borderColor: Int = Color.MAGENTA
) {
    Glide.with(context)
        .asBitmap()
        .load(model)
        .placeholder(R.drawable.ic_hipster)
        .apply(RequestOptions.circleCropTransform())
        .into(object : BitmapImageViewTarget(this) {
            override fun setResource(resource: Bitmap?) {
                setImageDrawable(
                    resource?.run {
                        RoundedBitmapDrawableFactory.create(
                            resources,
                            if (borderSize > 0) {
                                createBitmapWithBorder(borderSize, borderColor)
                            } else {
                                this
                            }
                        ).apply {
                            isCircular = true
                        }
                    }
                )
            }
        })
}

fun Bitmap.createBitmapWithBorder(borderSize: Float, borderColor: Int = Color.WHITE): Bitmap {
    val borderOffset = (borderSize * 2).toInt()
    val halfWidth = width / 2
    val halfHeight = height / 2
    val circleRadius = Math.min(halfWidth, halfHeight).toFloat()
    val newBitmap = Bitmap.createBitmap(
        width + borderOffset,
        height + borderOffset,
        Bitmap.Config.ARGB_8888
    )

    // Center coordinates of the image
    val centerX = halfWidth + borderSize
    val centerY = halfHeight + borderSize

    val paint = Paint()
    val canvas = Canvas(newBitmap).apply {
        // Set transparent initial area
        drawARGB(0, 0, 0, 0)
    }

    // Draw the transparent initial area
    paint.isAntiAlias = true
    paint.style = Paint.Style.FILL
    canvas.drawCircle(centerX, centerY, circleRadius, paint)

    // Draw the image
    paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
    canvas.drawBitmap(this, borderSize, borderSize, paint)

    // Draw the createBitmapWithBorder
    paint.xfermode = null
    paint.style = Paint.Style.STROKE
    paint.color = Color.parseColor("#869D95");
    paint.strokeWidth = borderSize
    canvas.drawCircle(centerX, centerY, circleRadius, paint)
    return newBitmap
}

fun EditText.setEndPositionCursor(){
    this.setSelection(this.text.length)
}

inline fun <reified T : View> View.find(id: Int): T = findViewById<T>(id)

fun Context?.sendBroadcastReceiver(intentFilter: IntentFilter, onReceive: (intent: Intent?) -> Unit): BroadcastReceiver {
    val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent?) {
            onReceive(intent)
        }
    }
    this?.registerReceiver(receiver, intentFilter)
    return receiver
}

fun EditText.isValidTextField(): Boolean{
    return !this.text.isEmpty() && this.text != null
}

fun String.isEmailValid(): Boolean {
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun getUUID(): String? {
    return UUID.randomUUID().toString()
}

fun getVersionApp(): String? {
    return getAppContext().packageManager.getPackageInfo(getAppContext().packageName, 0).versionName
}

