package com.ingenia.escobar.ui.scanner

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.afollestad.materialdialogs.MaterialDialog
import com.budiyev.android.codescanner.*
import com.ingenia.escobar.R
import com.ingenia.escobar.app.*
import com.ingenia.escobar.app.EscobarApp.Companion.getSharedString
import com.ingenia.escobar.databinding.ActivityScannerBinding
import com.ingenia.escobar.ui.detalle.Detalle
import com.ingenia.escobar.ui.manualscanner.ManualQR
import com.ingenia.escobar.viewmodel.QRViewModel
import com.ingeniapps.encargauser.utils.Status
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class Scanner : AppCompatActivity() {

    private lateinit var qrViewModel: QRViewModel
    private lateinit var binding: ActivityScannerBinding
    private lateinit var mCodeScanner: CodeScanner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityScannerBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        mCodeScanner = CodeScanner(this, binding.scannerView)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.navigationIcon = ResourcesCompat.getDrawable(resources, R.drawable.ic_logout, null)
        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = ""
        supportActionBar!!.elevation = 0f

        toolbar.setNavigationOnClickListener {
            showDialogCloseSession("¿Deseas cerrar tú sesión?",this)
        }

        qrViewModel = ViewModelProvider(this).get(QRViewModel::class.java)

    }

    override fun onResume() {
        super.onResume()
        checkPermissions()
    }

    fun initScanner(){
        mCodeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        mCodeScanner.formats = CodeScanner.ALL_FORMATS
        mCodeScanner.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        mCodeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        mCodeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not

        mCodeScanner.decodeCallback = DecodeCallback {
            runOnUiThread {
                mCodeScanner.stopPreview()
                lifecycleScope.launch(Dispatchers.Main) {
                    enableScreenWaitingRead(true)
                    if(it.text.isStringQR()){
                        readQR(it.text)
                    }
                    else{
                        readQR(it.text.getCedula().md5())
                    }
                }
            }
        }

        mCodeScanner.errorCallback = ErrorCallback { // or ErrorCallback.SUPPRESS
            runOnUiThread {
                Toast.makeText(this, "Camera initialization error: ${it.message}",
                    Toast.LENGTH_LONG).show()
            }
        }
        mCodeScanner.startPreview()

    }

    private fun enableScreenWaitingRead(showLoading: Boolean){
        if(showLoading){
            //mCodeScanner.stopPreview()
            binding.viewWaiting.visibility = View.VISIBLE
            binding.scanner.visibility = View.GONE
        }
        else{
            //mCodeScanner.startPreview()
            binding.viewWaiting.visibility = View.GONE
            binding.scanner.visibility = View.VISIBLE
        }
    }

    private fun showDialog(message: String?){
        val dialog = MaterialDialog(this)
            .icon(R.drawable.image_dialog)
            .title(text = message)
            .cancelable(false)
            .positiveButton(text = "Aceptar"){
                mCodeScanner.startPreview()
            }
        dialog.show()
    }

    private fun readQR(value: String) {
        qrViewModel.readQR(getSharedString("codUsuario"), value, 0)
            .observe(this, Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            resource.data?.let { it ->
                                if(it.status){
                                    enableScreenWaitingRead(false)
                                    val mp: MediaPlayer = MediaPlayer.create(this, R.raw.soundqr)
                                    mp.start()
                                    //INGRESO
                                    startActivity(intentFor<Detalle>("persona" to it.persona, "typeingreso" to it.typeIngreso))
                                    finish()
                                }
                                else{
                                    enableScreenWaitingRead(false)
                                    val mp: MediaPlayer = MediaPlayer.create(this, R.raw.error)
                                    mp.start()
                                    showDialog(it.message)
                                }
                            }
                        }
                        Status.ERROR -> {
                            val mp: MediaPlayer = MediaPlayer.create(this, R.raw.error)
                            mp.start()
                            enableScreenWaitingRead(false)
                            toast(""+it.message)

                        }
                        Status.LOADING -> {
                            enableScreenWaitingRead(true)
                        }
                    }
                }
            })
    }

    private fun checkPermissions(){
        Dexter.withActivity(this)
            .withPermission(Manifest.permission.CAMERA)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse) {
                    // permission is granted
                    initScanner()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse) {
                    // check for permanent denial of permission
                    if (response.isPermanentlyDenied) {
                        showSettingsDialog()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            }).withErrorListener { error ->
                Toast.makeText(applicationContext,"Error occurred! $error", Toast.LENGTH_SHORT).show()}
            .check()
    }

    fun showSettingsDialog(){
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GOTO SETTINGS") { dialog, _ ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton("Cancel") { dialog, _ ->
            dialog.cancel() }
        builder.show()
    }

    private fun openSettings(){
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_ingreso_manual, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_ingreso -> {
                startActivity<ManualQR>()
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onPause() {
        mCodeScanner.releaseResources()
        super.onPause()
    }
}