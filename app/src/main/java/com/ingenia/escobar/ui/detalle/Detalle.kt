package com.ingenia.escobar.ui.detalle

import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.ingenia.escobar.R
import com.ingenia.escobar.app.loadCircularImage
import com.ingenia.escobar.databinding.ActivityDetalleBinding
import com.ingenia.escobar.model.Persona
import com.ingenia.escobar.ui.scanner.Scanner
import org.jetbrains.anko.startActivity

class Detalle : AppCompatActivity() {

    private lateinit var binding: ActivityDetalleBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetalleBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val toolbar =
            findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = ""
        supportActionBar!!.elevation = 0f

        toolbar.setNavigationOnClickListener {
            startActivity<Scanner>()
            finish()
        }

        val persona = intent.getSerializableExtra("persona") as? Persona
        val typeingreso = intent.getStringExtra("typeingreso")
        binding.image.loadCircularImage(persona?.imgPersona)
        binding.cedula.text = "Id: ${persona?.numDocumento}"
        binding.name.text = persona?.nomPersona+" "+persona?.apePersona
        binding.telefono.text = "Tel: "+persona?.celPersona
        binding.oficio.text = "Oficio: "+persona?.nomFuncion
        binding.contratista.text = "Contratista: "+persona?.nomContratista
        binding.escanear.setOnClickListener {
            startActivity<Scanner>()
            finish()
        }

        if(typeingreso == "ingreso"){
            binding.typeingreso.text = "Ingreso"
            binding.typeingreso.setTextColor(Color.parseColor("#BBF619"));
        }
        else{
            binding.typeingreso.text = "Salida"
            binding.typeingreso.setTextColor(Color.parseColor("#f75165"));
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity<Scanner>()
            finish()
            true
        } else super.onKeyDown(keyCode, event)
    }
}