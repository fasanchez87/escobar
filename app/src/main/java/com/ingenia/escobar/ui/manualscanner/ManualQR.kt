package com.ingenia.escobar.ui.manualscanner

import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.ingenia.escobar.R
import com.ingenia.escobar.app.*
import com.ingenia.escobar.databinding.ActivityManualQrBinding
import com.ingenia.escobar.ui.detalle.Detalle
import com.ingenia.escobar.ui.scanner.Scanner
import com.ingenia.escobar.viewmodel.QRViewModel
import com.ingeniapps.encargauser.utils.Status
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivity

class ManualQR : AppCompatActivity() {

    private lateinit var qrViewModel: QRViewModel
    private lateinit var binding: ActivityManualQrBinding
    private var idCorrect = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityManualQrBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val toolbar =
            findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = ""
        supportActionBar!!.elevation = 0f

        toolbar.setNavigationOnClickListener {
            startActivity<Scanner>()
            finish()
        }

        qrViewModel = ViewModelProvider(this).get(QRViewModel::class.java)

        validateFields()

        binding.validaEnable.setOnClickListener {
            hideKeyboard(it)
            runOnUiThread {
                lifecycleScope.launch(Dispatchers.Main) {
                    enableScreenWaitingRead(true)
                    readQR(binding.cedula.text.toString().md5())
                }
            }
        }
    }

    private fun validateFields(){
        binding.cedula.afterTextChanged {
            idCorrect = false
            if(it != "" && it.length > 5){
                idCorrect = true
            }
            enableButtonValidate()
        }
    }

    private fun enableButtonValidate(){
        if(idCorrect){
            enableRegistro(true)
        }
        else{
            enableRegistro( false)
        }
    }

    private fun enableRegistro(enableButton: Boolean){
        if(enableButton){
            binding.validaDisable.visibility = View.GONE
            binding.validaEnable.visibility = View.VISIBLE
        }
        else{
            binding.validaDisable.visibility = View.VISIBLE
            binding.validaEnable.visibility = View.GONE
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity<Scanner>()
            finish()
            true
        } else super.onKeyDown(keyCode, event)
    }

    private fun enableScreenWaitingRead(showLoading: Boolean){
        if(showLoading){
            binding.viewWaiting.visibility = View.VISIBLE
            binding.scanner.visibility = View.GONE
        }
        else{
            binding.viewWaiting.visibility = View.GONE
            binding.scanner.visibility = View.VISIBLE
        }
    }

    private fun clearFields(){
        binding.cedula.text = null
    }

    private fun readQR(value: String) {
        qrViewModel.readQR(EscobarApp.getSharedString("codUsuario"), value, 1)
            .observe(this, Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            resource.data?.let { it ->
                                if(it.status){
                                    clearFields()
                                    enableScreenWaitingRead(false)
                                    val mp: MediaPlayer = MediaPlayer.create(this, R.raw.soundqr)
                                    mp.start()
                                    //INGRESO
                                    if(it.persona != null){
                                        startActivity(intentFor<Detalle>("persona" to it.persona, "typeingreso" to it.typeIngreso))
                                        finish()
                                    }
                                }
                                else{
                                    showDialog(it.message)
                                    enableScreenWaitingRead(false)
                                    val mp: MediaPlayer = MediaPlayer.create(this, R.raw.error)
                                    mp.start()
                                }
                            }
                        }
                        Status.ERROR -> {
                            val mp: MediaPlayer = MediaPlayer.create(this, R.raw.error)
                            mp.start()
                            enableScreenWaitingRead(false)
                            toast(it.message)
                        }
                        Status.LOADING -> {
                            enableScreenWaitingRead(true)
                        }
                    }
                }
            })
    }
}