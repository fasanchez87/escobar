package com.ingenia.escobar.ui.splash

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.ingenia.escobar.R
import com.ingenia.escobar.app.setWindowFullScreen
import com.ingenia.escobar.ui.login.Login
import org.jetbrains.anko.startActivity

class Splash : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setWindowFullScreen()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            startActivity<Login>()
            finish()
        }, 3000)
    }
}