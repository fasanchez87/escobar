package com.ingenia.escobar.ui.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.ingenia.escobar.R
import com.ingenia.escobar.app.*
import com.ingenia.escobar.app.EscobarApp.Companion.getSharedBoolean
import com.ingenia.escobar.app.EscobarApp.Companion.putSharedBoolean
import com.ingenia.escobar.app.EscobarApp.Companion.putSharedString
import com.ingenia.escobar.databinding.ActivityLoginBinding
import com.ingenia.escobar.ui.scanner.Scanner
import com.ingenia.escobar.viewmodel.LoginViewModel
import com.ingeniapps.encargauser.utils.Status
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.startActivity

class Login : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel

    private lateinit var binding: ActivityLoginBinding
    private var userCorrect = false
    private var passCorrect = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(getSharedBoolean("isSesionActive")){
            chargueHomeActivity()
            return
        }

        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        binding.loginEnable.setOnClickListener {
            hideKeyboard(it)
            lifecycleScope.launch(Dispatchers.Main) {
                enableRegistro(false)
                enableFields(false)
                login()
            }
            /*startActivity<Scanner>()
            finish()*/
        }

        validateFields()
        eyeSeePasswordConfig()



    }

    fun eyeSeePasswordConfig(){
        binding.showPassBtn.setOnClickListener {
            if (binding.clave.transformationMethod == PasswordTransformationMethod.getInstance()) {
                binding.showPassBtn.setImageResource(R.drawable.ic_eye);
                //Show Password
                binding.clave.transformationMethod = HideReturnsTransformationMethod.getInstance();
                binding.clave.setEndPositionCursor()
                hideKeyboard(it)
            } else {
                binding.showPassBtn.setImageResource(R.drawable.ic_eye_closed);
                //Hide Password
                binding.clave.transformationMethod = PasswordTransformationMethod.getInstance();
                binding.clave.setEndPositionCursor()
                hideKeyboard(it)
            }
        }
    }

    private fun validateFields(){
        binding.user.afterTextChanged {
            userCorrect = false
            if(it != ""){
                userCorrect = true
            }
            enableButtonRegister()
        }

        binding.clave.afterTextChanged {
            passCorrect = false
            if(it != "" && it.length > 5){
                passCorrect = true
            }
            enableButtonRegister()
        }
    }

    private fun enableButtonRegister(){
        if(userCorrect && passCorrect){
            enableRegistro(true)
        }
        else{
            enableRegistro( false)
        }
    }
    private fun chargueHomeActivity(){
        startActivity<Scanner>()
        finish()
    }

    private fun enableRegistro(enableButton: Boolean){
        if(enableButton){
            binding.loginDisable.visibility = View.GONE
            binding.loginEnable.visibility = View.VISIBLE
        }
        else{
            binding.loginDisable.visibility = View.VISIBLE
            binding.loginEnable.visibility = View.GONE
        }
    }

    private fun enableFields(enableFields: Boolean){
        binding.user.isEnabled = enableFields
        binding.clave.isEnabled = enableFields
        binding.showPassBtn.isEnabled = enableFields
    }

    private fun login() {
        loginViewModel.login(binding.user.text.toString(),binding.clave.text.toString())
            .observe(this, Observer {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            enableFields(true)
                            enableRegistro(true)
                            binding.loading.visibility = View.INVISIBLE
                            resource.data?.let { it ->
                                if(it.status){
                                    putSharedBoolean("isSesionActive", true)
                                    putSharedString("codUsuario", it.data.codUsuario)
                                    chargueHomeActivity()
                                }
                                else{
                                    toast(it.message)
                                }
                            }
                        }
                        Status.ERROR -> {
                            enableFields(true)
                            enableRegistro(true)
                            binding.loading.visibility = View.INVISIBLE
                            Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                        }
                        Status.LOADING -> {
                            binding.loading.visibility = View.VISIBLE
                            enableRegistro(false)
                            enableFields(false)
                        }
                    }
                }
            })
    }
}