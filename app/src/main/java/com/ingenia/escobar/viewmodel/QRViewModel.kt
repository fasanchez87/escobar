package com.ingenia.escobar.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.ingenia.escobar.R
import com.ingenia.escobar.app.EscobarApp
import com.ingenia.escobar.app.Injection
import com.ingenia.escobar.model.ResultReadQR
import com.ingeniapps.encargauser.utils.Resource
import com.ingeniapps.encargauser.utils.handleErrorAPI
import kotlinx.coroutines.Dispatchers
import retrofit2.HttpException

class QRViewModel(application: Application): AndroidViewModel(application) {

    private val repository = Injection.provideRepository()

    fun readQR(codUsuario: String?, codQr: String, indManual: Int): LiveData<Resource<ResultReadQR>> {
        return liveData(Dispatchers.IO) {
            emit(Resource.loading(data = null))
            try {
                emit(Resource.success(data = repository.readQR(codUsuario, codQr, indManual)))
            }
            catch (exception: HttpException) {
                emit(Resource.error(data = null, message = handleErrorAPI(exception)))
            }
            catch (exception: Exception) {
                emit(Resource.error(data = null, message = EscobarApp.getAppContext().getString(R.string.exception_msg_connection)))
            }
        }
    }
}